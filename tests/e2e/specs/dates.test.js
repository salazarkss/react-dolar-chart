import { getRandomInt } from '../support/index';

const setRoutes = () => {
  cy.server();
  cy.route(
    'GET',
    'https://api.sbif.cl/api-sbifv3/recursos_api/dolar/**'
  ).as('getDolarInfo');
};

describe('Dates Tests', () => {

  beforeEach(() => {
    cy.visit(Cypress.env('baseUrl'));
    setRoutes();
  });

  it('Test Initial Date Change', () => {
    // Get random month amount
    const month = getRandomInt(0, 12);
    // Get random year amount
    const year = getRandomInt(0, 10);
    // Get random day to click
    const day = getRandomInt(1, 30);
    // Click datepicker, so, calendar is showed
    cy.get('.home-date-picker').eq(0).click();
    // Iterate by random month
    for (let i = 0; i < month; i++) {
      cy.contains('‹').click();
    }
    // Iterate by random year
    for (let i = 0; i < year; i++) {
      cy.contains('«').click();
    }
    // Click random day
    cy.contains(day.toString()).click();
    // Wait for data refresh. Expect response status === 200
    cy.wait('@getDolarInfo', 20000).its('status').should('be.equal', 200);
  });

  it('Test Final Date Limitations', () => {
    // Create today date
    const today = new Date();
    // Create and set oneMonthAgo date
    const oneMonthAgo = new Date();
    oneMonthAgo.setDate(oneMonthAgo.getDate() - 30);
    // Get final date picker
    cy.get('.home-date-picker').eq(1).click();
    // Get if today plus a day is not visible at date picker
    cy.contains(today.getDate() + 1).should('be.disabled');
    // Click on prev Month, for check oneMonthAgo info
    cy.get('.home-date-picker').eq(1).contains('‹').click();
    // Get if oneMonthAgo less a day is not visible. This means that minFinalDate === initialDate
    cy.get('.home-date-picker').eq(1).contains(oneMonthAgo.getDate() - 2).should('be.disabled');
  });

});