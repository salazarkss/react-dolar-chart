describe('Page Test', () => {

  before(() => {
    cy.visit(Cypress.env('baseUrl'));
  });

  it('Test Initial Page Load', () => {
    // Get if dashboard container is visible
    cy.get('.dash-container').should('be.visible');
    // Get if datepickers are 2. Initial date and final date.
    cy.get('.home-date-picker').should('have.length', 2);
  });
});