import React from 'react';
import ReactDOM from 'react-dom';
import './index.sass';
import 'fontsource-roboto';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
