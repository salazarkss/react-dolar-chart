import axios from 'axios';

const baseUrl = 'https://api.sbif.cl/api-sbifv3/recursos_api/dolar/periodo';
const apiKey = '9c84db4d447c80c74961a72245371245cb7ac15f';


function getDolarsValueByDates(dates) {
  const initialDateAsArray = dates[0].split('-').reverse();
  const finalDateAsArray = dates[1].split('-').reverse();
  const initialDateInfo = `/${initialDateAsArray[0]}/${initialDateAsArray[1]}/dias_i/${initialDateAsArray[2]}`;
  const finalDateInfo = `/${finalDateAsArray[0]}/${finalDateAsArray[1]}/dias_f/${finalDateAsArray[2]}`;
  return axios.get(`${baseUrl}${initialDateInfo}${finalDateInfo}?apikey=${apiKey}&formato=json`);
}

export default getDolarsValueByDates;