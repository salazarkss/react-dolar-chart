import React from 'react'
import { render } from '@testing-library/react';
import Dashboard from './Dashboard.js';
import 'jest-canvas-mock';
import '@testing-library/jest-dom'

const yyyymmdd = date => {
  if (date) {
    let month = date.getMonth() + 1;
    month = (month > 9 ? '' : '0') + month;
    let day = date.getDate();
    day = (day > 9 ? '' : '0') + day;
    return [date.getFullYear(), month, day].join('-');
  }
}

describe('Unit Testing Dashboard Component', () => {
  it('Test Dashboard Render', () => {
    const today = new Date();
    const oneMonthAgo = new Date();
    oneMonthAgo.setDate(oneMonthAgo.getDate() - 30);
    render(<Dashboard initialDate={yyyymmdd(oneMonthAgo)} finalDate={yyyymmdd(today)}/>);
  });
});
