import React from 'react';
import CanvasJSReact from '../assets/canvasjs.react.js';
import getDolarsValueByDates from '../api/dolarRequests.js';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataPoints: [],
      loading: false,
      currentInitialDate: this.props.initialDate,
      currentFinalDate: this.props.finalDate,
      max: 0,
      min: 0,
      mean: 0,
      isMobile: window.innerWidth < 425
    };
  }

  getDateFormat(date) {
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    return `${day}-${month}-${year}`;
  }

  getMaxValue() {
    const max = this.state.dataPoints.reduce((acc, val) => {
      if (acc < val.y) {
        acc = val.y;
      }
      return acc;
    }, 0);
    this.setState({
      max,
    });
  }

  getMinValue() {
    const min = this.state.dataPoints.reduce((acc, val) => {
      if (acc === 0) {
        acc = val.y;
      } else {
        if (acc > val.y) {
          acc = val.y
        }
      }
      return acc;
    }, 0);
    this.setState({
      min,
    });
  }

  getMeanValue() {
    const sum = this.state.dataPoints.reduce((acc, val) => {
      return acc + val.y;
    }, 0);
    this.setState({
      mean: (sum/this.state.dataPoints.length).toFixed(2)
    });
  }

  updateValues() {
    this.getMaxValue();
    this.getMinValue();
    this.getMeanValue();
  }

  componentDidMount() {
    this.getDatesData();
  }

  componentDidUpdate() {
    const initialDateChange = this.props.initialDate !== this.state.currentInitialDate;
    const finalDateChange = this.props.finalDate !== this.state.currentFinalDate;
    if (initialDateChange || finalDateChange) {
      this.setState({
        currentInitialDate: this.props.initialDate,
        currentFinalDate: this.props.finalDate
      });
      this.getDatesData();
    }
  }

  getDatesData() {
    this.setState({
      loading: true
    });
    const initialDate = new Date(...this.props.initialDate.split('-'));
    const finalDate = new Date(...this.props.finalDate.split('-'));
    const dates = [];
    dates.push(this.getDateFormat(initialDate));
    dates.push(this.getDateFormat(finalDate));
    Promise.resolve(getDolarsValueByDates(dates)).then(response => {
      const dolarData = response.data.Dolares.map(dolar => {
        return {
          x: new Date(dolar.Fecha),
          y: parseInt(dolar.Valor),
          color: '#ffec00'
        };
      });
      this.setState({
        dataPoints: dolarData,
        loading: false
      });
      this.updateValues();
    });
  }

  render() {
    const options = {
      animationEnabled: true,
      animationDuration: 4000,
      theme: 'dark2',
      backgroundColor: 'transparent',
      zoomEnabled: true,
      rangeChanged: e => {},
      title: {
        text: 'Dolar over time',
        fontFamily: 'Roboto',
        fontWeight: 'bold'
      },
      axisX: {
        valueFormatString: "DD-MMM" ,
        labelAngle: -50
      },
      axisY: {
        includeZero: false
      },
      legend: {
        fontFamily: 'Roboto',
        fontWeight: '300',
        fontSize: 16
      },
      toolTip: {
        cornerRadius: 8,
        backgroundColor: '#1e2f51',
        fontColor: 'white',
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        borderColor: '#ffec00',
        borderThickness: 2
      },
      data: [{
        type: 'line',
        toolTipContent: '{x}:  ${y}',
        dataPoints: this.state.dataPoints,
        lineColor: 'white',
        cursor: 'pointer',
        showInLegend: !this.state.isMobile,
        legendText: `Maximo: ${this.state.max} Minimo: ${this.state.min} Promedio ${this.state.mean}`
      }]
    };
    return (
      <div className="dash-container" data-testid="dolar-dashboard">
        <CanvasJSChart
          options={options}/>
      </div>
    );
  }
}

export default Dashboard;