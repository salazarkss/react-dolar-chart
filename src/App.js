import React from 'react';
import Dashboard from './components/Dashboard.js';
import DatePicker from 'react-date-picker';

class App extends React.Component {

  constructor(props) {
    super(props);
    const today = new Date();
    const oneMonthAgo = new Date();
    oneMonthAgo.setDate(oneMonthAgo.getDate() - 30);
    this.state = {
      initialDateValue: oneMonthAgo,
      finalDateValue: today,
      dashInitialDate: this.yyyymmdd(oneMonthAgo),
      dashFinalDate: this.yyyymmdd(today)
    };
    this.initialDateChange = this.initialDateChange.bind(this);
    this.finalDateChange = this.finalDateChange.bind(this);
  }

  yyyymmdd(date) {
    if (date) {
      let month = date.getMonth() + 1;
      month = (month > 9 ? '' : '0') + month;
      let day = date.getDate();
      day = (day > 9 ? '' : '0') + day;
      return [date.getFullYear(), month, day].join('-');
    }
  }

  initialDateChange(date) {
    this.setState({
      initialDateValue: date,
      dashInitialDate: this.yyyymmdd(date)
    });
  }

  finalDateChange(date) {
    this.setState({
      finalDateValue: date,
      dashFinalDate: this.yyyymmdd(date)
    });
  }

  render() {
    return (
      <div className="mainContainer">
        <div className="columns is-multiline">
          <div className="column is-full">
            <Dashboard
              initialDate={this.state.dashInitialDate}
              finalDate={this.state.dashFinalDate}/>
          </div>
          <div className="column is-full-mobile is-one-quarter-desktop date-column">
            <span>Desde: </span>
            <DatePicker
              value={this.state.initialDateValue}
              maxDate={this.state.finalDateValue}
              onChange={this.initialDateChange}
              clearIcon={null}
              className="home-date-picker"/>
          </div>
          <div className="column is-full-mobile is-one-quarter-desktop date-column">
            <span>Hasta: </span>
            <DatePicker
              value={this.state.finalDateValue}
              onChange={this.finalDateChange}
              minDate={this.state.initialDateValue}
              maxDate={new Date()}
              clearIcon={null}
              className="home-date-picker"/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
